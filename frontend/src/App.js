import logo from './logo.svg';
import './App.css';
import React, { Component } from "react";
// import "bootswatch/journal/bootstrap.css";

//import axios from axios;


const PLACES = [
  { name: "Moscow", zip: "101000" },
  { name: "Saint Petersburg", zip: "187015" },
  { name: "Svetlograd", zip: "356530" },
  { name: "Smolensk", zip: "214000" }
];

class WeatherDisplay extends Component {
  constructor() {
    super();
    this.state = {
      weatherData: null
    };
  }
  componentDidMount() {
    const zip = this.props.zip;
    const URL = "http://api.openweathermap.org/data/2.5/weather?q=" +
      zip +
      "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=imperial";
    fetch(URL).then(res => res.json()).then(json => {
      this.setState({ weatherData: json });
    });
  }
  render() {
    const weatherData = this.state.weatherData;
    if (!weatherData) return <div>Loading</div>;
    const weather = weatherData.weather[0];
    const iconUrl = "http://openweathermap.org/img/w/" + weather.icon + ".png";
    return (
      <div>
        <h1>
          {weather.main} in {weatherData.name}
          <img src={iconUrl} alt={weatherData.description} />
        </h1>
        <p>Current: {weatherData.main.temp}°</p>
        <p>High: {weatherData.main.temp_max}°</p>
        <p>Low: {weatherData.main.temp_min}°</p>
        <p>Wind Speed: {weatherData.wind.speed} mi/hr</p>
      </div>
    );
  }
}


// function App() {
//   return (
//     <div className="App">
//       {PLACES.map((place, index) => (
//           <button
//             key={index}
//             onClick={() => {
//               console.log("Clicked index " + index);
//             }}
//           >
//             {place.name}
//           </button>
//         ))};

//       <WeatherDisplay zip={"12345"} />
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          !!Edit <code>src/App.js</code> and save to reload. !!!CHANGE!!!
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      
      <button onClick={makeApiRequests}>Make api requests</button> */}
//     </div>
//   );
// }

class App extends Component {
  constructor() {
    super();
    this.state = {
      activePlace: 0
    };
  }
  render() {
    const activePlace = this.state.activePlace;
    return (
      <div className="App">
        {PLACES.map((place, index) => (
          <button
            key={index}
            onClick={() => {
              this.setState({ activePlace: index });
            }}
          >
            {place.name}
          </button>
        ))}
        <WeatherDisplay key={activePlace} zip={PLACES[activePlace].zip} />
      </div>
    );
  }
}

export default App;
