const express = require('express');
const axios = require('axios');
const { connectDb } = require('./helpers/db');
const app = express();
const {host, port, db, apiUrl} = require("./configuration")


// const port = process.env.PORT;
// const host = process.env.HOST;

const startServer = () => {
    app.listen(port, () => {
        console.log(`started auth on port ${port}`);
        console.log(`On host ${host}`);
        console.log(`Our database ${db}`)
    });
};

// console.log("PORT", process.env.PORT);

app.get('/test', (req, res) => {
    res.send('Our auth is working correctly')
});

app.get('/testwithapidata', (req, res) => {
    axios.get(apiUrl + '/testapidata').then(response => {
        res.json({
            testapidata: response.data.testwithapi
        });
    });
});

app.get("/api/currentUser", (req,res) => {
    res.json({
        id: "1234",
        email: "foo@gmail.com"
    });
});

connectDb()
    .on('error', console.log)
    .on("disconnected", connectDb)
    .on("open", startServer)