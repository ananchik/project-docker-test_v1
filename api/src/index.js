const express = require('express');
const mongoose = require('mongoose');
const axios = require('axios');
const { connectDb } = require('./helpers/db');
const app = express();
const {host, port, db, authApiUrl} = require("./configuration");
const { response } = require('express');
const postSchema = new mongoose.Schema({
    name: String
  });
const Post = mongoose.model('Post', postSchema);

// const port = process.env.PORT;
// const host = process.env.HOST;

const startServer = () => {
    app.listen(port, () => {
        console.log(`started api service on port ${port}`);
        console.log(`On host ${host}`);
        console.log(`Our database ${db}`)

        const silence = new Post({ name: 'Silence' });
        silence.save(function(err, savedSilience) {
            if (err) return console.error(err);
            console.log('savedSilience', savedSilience)
        });
    });
};

// console.log("PORT", process.env.PORT);

app.get('/test', (req, res) => {
    res.send('Our api server is working correctly')
});

app.get('/api/testapidata', (req, res) => {
    res.json({
        testwithapi: true
    });
});

app.get("/testWithCurrentUser", (req, res)=> {
    // console.log('authApiUrl', authApiUrl);
    axios.get(authApiUrl + '/currentUser').then(response =>{
        res.json({
            testWithCurrentUser: true,
            currentUserFromAuth: response.data
        });
    });
});

connectDb()
    .on('error', console.log)
    .on("disconnected", connectDb)
    .on("open", startServer)